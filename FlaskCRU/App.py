from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL



app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'crud'

mysql = MySQL(app)



@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute("SELECT  * FROM sensores")
    data = cur.fetchall()
    cur.close()




    return render_template('index.html', sensores=data )



@app.route('/insert', methods = ['POST'])
def insert():

    if request.method == "POST":
        flash("Dados inseridos com sucesso")
        Sensor = request.form['sensor']
        Valor = request.form['valor']
        Praia = request.form['praia']
        Cidade = request.form['cidade']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO sensores (sensor, valor, praia, cidade) VALUES (%s, %s, %s, %s)", (Sensor, Valor, Praia, Cidade))
        mysql.connection.commit()
        return redirect(url_for('Index'))




@app.route('/delete/<string:id_data>', methods = ['GET'])
def delete(id_data):
    flash("O registro foi excluído com sucesso")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM sensores WHERE id=%s", (id_data,))
    mysql.connection.commit()
    return redirect(url_for('Index'))





@app.route('/update',methods=['POST','GET'])
def update():

    if request.method == 'POST':
        id_data = request.form['id']
        Sensor = request.form['sensor']
        Valor = request.form['valor']
        Praia = request.form['praia']
        Cidade = request.form['cidade']
        cur = mysql.connection.cursor()
        cur.execute("""
               UPDATE sensores
               SET sensor=%s, valor=%s, praia=%s, cidade=%s
               WHERE id=%s
            """, (Sensor, Valor, Praia, Cidade, id_data))
        flash("Sucesso Inserido")
        mysql.connection.commit()
        return redirect(url_for('Index'))









if __name__ == "__main__":
    app.run(debug=True)
